package com.example.demo.controller;

import com.example.demo.model.dto.FileReq;
import com.example.demo.service.seriveInterfaces.FileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/file")
@RequiredArgsConstructor
@Slf4j
public class FileController {

    private final FileService fileService;

    @GetMapping
    public void getFileInfoV2(@RequestPart("file") MultipartFile file) {
        fileService.getFileInfo(file);
    }

    @GetMapping("/v2")
    public void getFileInfoV2(FileReq fileReq) {
        fileService.getFileInfoV2(fileReq);
    }
    @PostMapping(value = "/uploadFile", consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> uploadFile(@RequestPart(value = "file") FileReq fileReq) {
        return fileService.uploadFile(fileReq);
    }
}
