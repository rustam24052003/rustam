package com.example.demo.model.mapper;

import com.example.demo.model.dto.AddressDto;
import com.example.demo.model.dto.EmployeeDto;
import com.example.demo.model.entity.AddressEntity;
import com.example.demo.model.entity.EmployeeEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-05-08T15:26:06+0400",
    comments = "version: 1.5.5.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-8.6.jar, environment: Java 17.0.9 (Oracle Corporation)"
)
@Component
public class EmployeeMapperImpl implements EmployeeMapper {

    @Override
    public EmployeeDto employeeToEmployeeDTO(EmployeeEntity employee) {
        if ( employee == null ) {
            return null;
        }

        EmployeeDto.EmployeeDtoBuilder employeeDto = EmployeeDto.builder();

        employeeDto.id( employee.getId() );
        employeeDto.firstname( employee.getFirstname() );
        employeeDto.lastname( employee.getLastname() );
        employeeDto.age( employee.getAge() );
        employeeDto.salary( employee.getSalary() );

        return employeeDto.build();
    }

    @Override
    public AddressDto addressToAddressDTO(AddressEntity address) {
        if ( address == null ) {
            return null;
        }

        AddressDto.AddressDtoBuilder addressDto = AddressDto.builder();

        return addressDto.build();
    }
}
