package com.example.demo.model.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "departments")
public class DepartmentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "department_generator")
    @SequenceGenerator(name = "department_generator",sequenceName = "departments_seq",allocationSize = 1)
    Long id;

    @Column(name = "department_name")
    String name;


    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    List<EmployeeEntity> employeeEntity;

}
