package com.example.demo.service.serviceInterfaces;

import com.example.demo.model.dto.DepartmentDto;
import com.example.demo.model.dto.fullDto.FullDepartmentDto;

import java.util.List;

public interface DepartmentService {
    List<FullDepartmentDto> getAllDepartments();

    Boolean addDepartment(FullDepartmentDto fullDepartmentDto);

    List<DepartmentDto> getDepartmentsStartWith(String letter);
}
