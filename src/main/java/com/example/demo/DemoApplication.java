package com.example.demo;


import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	private static final Logger logger = Logger.getLogger(DemoApplication.class.getName());
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		System.out.println("New branch lesson36 upgrade");
	}

	@Override
	public void run(String... args) throws Exception {
		logger.setLevel(Level.INFO);
		logger.info("This is an info message.");
		logger.warning("This is a warning message.");
		logger.severe("This is a severe message.");
	}
}

