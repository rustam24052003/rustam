package com.example.demo.model.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "address")
public class AddressEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "address_generator")
    @SequenceGenerator(name = "address_generator", sequenceName = "address_seq", allocationSize = 1)
    Long id;

    @Column(name = "address_name")
    String addressName;

    @OneToOne(mappedBy = "addressEntity", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    EmployeeEntity employeeEntity;
}
