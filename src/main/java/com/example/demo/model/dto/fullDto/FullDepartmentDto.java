package com.example.demo.model.dto.fullDto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FullDepartmentDto {

    String name;

    List<FullEmployeeDto> fullEmployeeDto;
}
