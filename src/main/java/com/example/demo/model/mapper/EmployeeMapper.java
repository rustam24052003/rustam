package com.example.demo.model.mapper;

import com.example.demo.model.dto.AddressDto;
import com.example.demo.model.dto.EmployeeDto;
import com.example.demo.model.entity.AddressEntity;
import com.example.demo.model.entity.EmployeeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {
    EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);

    EmployeeDto employeeToEmployeeDTO(EmployeeEntity employee);
    AddressDto addressToAddressDTO(AddressEntity address);
}