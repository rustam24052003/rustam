package com.example.demo.controller;



import com.example.demo.model.dto.DepartmentDto;
import com.example.demo.model.dto.fullDto.FullDepartmentDto;
import com.example.demo.service.serviceInterfaces.DepartmentService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/department")
@RequiredArgsConstructor
public class DepartmentController {
    private final DepartmentService departmentService;

    @PostMapping("/addDepartment")
    public Boolean addDepartment(@RequestBody FullDepartmentDto fullDepartmentDto) {
        return departmentService.addDepartment(fullDepartmentDto);
    }

    @GetMapping("/getAllDepartments")
    public List<FullDepartmentDto> getAllDepartments() {
        return departmentService.getAllDepartments();
    }

    @GetMapping("/getByLetter")
    public List<DepartmentDto> getDepartmentByLetter(@RequestParam("letter") String letter) {
        return departmentService.getDepartmentsStartWith(letter);
    }
}
